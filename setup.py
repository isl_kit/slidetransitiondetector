#!/usr/bin/python
# -*- coding: latin-1 -*-

from distutils.core import setup

setup(
    name="SlideTransitionDetector",
    version="1.1.0",
    author="Rene Brandel",
    packages=["slidetransitiondetector"],
    scripts=["bin/detect_slides.py", "bin/sort_slides.py", "bin/extract_content.py"],
    url="https://bitbucket.org/isl_kit/slidetransitiondetector.git",
    license="LICENSE.txt",
    description="A python tool, that detects and extracts slides from a lecture video.",
    long_description=open("README.md").read(),
    install_requires=[
        "progressbar>=2.3",
        "Pillow>=3.2.0",
        "numpy>=1.13.1",
        "scipy>=0.19.1",
        "pyocr>=0.4.2"
    ],
    dependency_links = [
       "git+https://gitlab.gnome.org/World/OpenPaperwork/pyocr.git"
    ],
    zip_safe = False,
)

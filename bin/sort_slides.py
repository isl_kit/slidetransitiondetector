#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import os
import argparse

from slidetransitiondetector.sources import ListSource
from slidetransitiondetector.slides import SlideDataHelper
from slidetransitiondetector.sorter import SlideSorter


if __name__ == '__main__':

    Parser = argparse.ArgumentParser(description="Slide Sorter")
    Parser.add_argument("-d", "--inputslides", help="path of the sequentially sorted slides", default="slides/")
    Parser.add_argument("-o", "--outpath", help="path to output slides", default="unique/", nargs='?')
    Parser.add_argument("-f", "--fileformat", help="file format of the output images e.g. '.jpg'",
                        default=".jpg", nargs='?')
    Parser.add_argument("-t", "--timetable",
                        help="path where the timetable should be written (default is the outpath+'timetable.txt')",
                        nargs='?', default=None)
    Args = Parser.parse_args()
    if Args.timetable is None:
        Args.timetable = os.path.join(Args.outpath, "timetable.txt")

    sorter = SlideSorter(ListSource(SlideDataHelper(Args.inputslides).get_slides()), Args.outpath, Args.timetable, Args.fileformat)
    sorter.sort()

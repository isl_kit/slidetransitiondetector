#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import argparse

from slidetransitiondetector.detector import Detector

if __name__ == '__main__':
    Parser = argparse.ArgumentParser(description='Slide Detector')
    Parser.add_argument('-d', '--device', help='video device number or path to video file')
    Parser.add_argument('-o', '--outpath', help='path to output video file', default='slides/', nargs='?')
    Parser.add_argument('-f', '--fileformat', help='file format of the output images e.g. .jpg', default='.jpg', nargs='?')
    Args = Parser.parse_args()
    detector = Detector(Args.device, Args.outpath, Args.fileformat)
    detector.detect_slides()

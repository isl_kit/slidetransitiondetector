#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import argparse

from slidetransitiondetector.slides import SlideDataHelper
from slidetransitiondetector.sources import ListSource
from slidetransitiondetector.extractor import ContentExtractor


if __name__ == "__main__":
    Parser = argparse.ArgumentParser(description="Slide Sorter")
    Parser.add_argument("-d", "--inputslides", help="path of the sequentially sorted slides", default="unique/")
    Parser.add_argument("-o", "--outpath", help="path to output the content of the slides", default="contents/", nargs='?')
    Parser.add_argument("-l", "--lang", help="language to be analyzed", default="eng", nargs='?')
    Args = Parser.parse_args()
    ContentExtractor(ListSource(SlideDataHelper(Args.inputslides).get_slides()), Args.outpath, lang=Args.lang).analyze()

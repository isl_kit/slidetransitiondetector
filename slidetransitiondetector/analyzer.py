from abc import ABCMeta, abstractmethod


class Analyzer(object, metaclass=ABCMeta):
    @abstractmethod
    def analyze(self):
        pass
